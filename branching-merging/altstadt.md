# Altstadt

### Soban

Zwingerstr. 21, Heidleberg, BW 69117

#### Pro

- good, spicy Korean food

#### Con

- not many vegetarian/vegan options
- quite small - best to make a reservation!